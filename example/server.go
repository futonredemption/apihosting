package example

import (
	"bitbucket.org/futonredemption/apihosting/apiserving"
	pb "bitbucket.org/futonredemption/apihosting/example/proto/futonredemption"
	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
)

type serverCertBundle struct {
	rootCaPublicPem  []byte
	rootCaPrivatePem []byte
	serverPublicPem  []byte
	serverPrivatePem []byte
}

type exampleService struct {
}

func (m *exampleService) GetExample(c context.Context, s *pb.GetExampleRequest) (*pb.GetExampleResponse, error) {
	return &pb.GetExampleResponse{
		Val: "GET " + s.Val,
	}, nil
}

func (m *exampleService) PostExample(c context.Context, s *pb.PostExampleRequest) (*pb.PostExampleResponse, error) {
	return &pb.PostExampleResponse{
		Val: "POST " + s.Val,
	}, nil
}

func newExampleService() *exampleService {
	return &exampleService{}
}

func SetupServer(bundle *serverCertBundle, enableInsecure, enableSecure bool, basePort int) apiserving.ApiServer {
	serviceHandler := newExampleService()
	server := apiserving.New().
		SetSwaggerString(pb.ExampleSwaggerJson).
		SetCertificatesFromPEM(bundle.serverPublicPem, bundle.serverPrivatePem).
		SetRootCertificateFromPEM(bundle.rootCaPublicPem).
		Add(func(server *grpc.Server) {
			pb.RegisterExampleServiceServer(server, serviceHandler)
		}).
		AddRest(func(ctx context.Context, mux *runtime.ServeMux, endpoint string, opts []grpc.DialOption) error {
			return pb.RegisterExampleServiceHandlerFromEndpoint(ctx, mux, endpoint, opts)
		}).
		EnableMetrics().
		EnableDebug()
	if enableInsecure {
		server.SetHttpPort(basePort).SetGrpcPort(basePort + 1)
	}
	if enableSecure {
		server.SetHttpsPort(basePort + 2).SetTlsGrpcPort(basePort + 3)
	}

	return server
}
