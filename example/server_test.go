package example

import (
	"bitbucket.org/futonredemption/apihosting/apitesting"
	"github.com/stretchr/testify/assert"
	"testing"

	pb "bitbucket.org/futonredemption/apihosting/example/proto/futonredemption"
	"crypto/tls"
	"crypto/x509"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
)

func TestServerConstruction(t *testing.T) {
	assert := assert.New(t)
	server := SetupServer(TEST_CERT_BUNDLE, false, false, nextPort())
	assert.NotNil(server)
}

func TestEmptyServer(t *testing.T) {
	assert := assert.New(t)
	server := SetupServer(TEST_CERT_BUNDLE, false, false, nextPort())

	err := server.BeginServing()
	assert.Nil(err)
	err = server.WaitForServing()
	assert.Nil(err)
	server.StopServing()
	assert.NotNil(server)
}

func TestExampleServerAll(t *testing.T) {
	assert := assert.New(t)
	port := nextPort()
	server := SetupServer(TEST_CERT_BUNDLE, true, true, port)

	certPool, err := getCertPoolForTest()
	assert.Nil(err)

	err = server.BeginServing()
	assert.Nil(err)
	err = server.WaitForServing()
	assert.Nil(err)
	apitesting.HttpGetCompareBody(t, setHttpPort("http://localhost:$PORT/v1/getExample", port), "GET")
	apitesting.HttpGetCompareBody(t, setHttpPort("http://localhost:$PORT/swagger.json", port), "futonredemptionGetExampleResponse")
	apitesting.HttpsGetCompareBody(t, setHttpsPort("https://localhost:$PORT/v1/getExample", port), certPool, "GET")
	apitesting.HttpsGetCompareBody(t, setHttpsPort("https://localhost:$PORT/swagger.json", port), certPool, "futonredemptionGetExampleResponse")

	grpcInsecureTrial(assert, setGrpcPort("localhost:$PORT", port))

	grpcTlsTrial(assert, setGrpcTlsPort("localhost:$PORT", port), certPool)
	grpcTlsTrial(assert, setHttpsPort("localhost:$PORT", port), certPool)

	server.StopServing()
	assert.NotNil(server)
}

func TestExampleServerInsecure(t *testing.T) {
	assert := assert.New(t)
	port := nextPort()
	server := SetupServer(TEST_CERT_BUNDLE, true, false, port)

	err := server.BeginServing()
	assert.Nil(err)
	err = server.WaitForServing()
	assert.Nil(err)
	apitesting.HttpGetCompareBody(t, setHttpPort("http://localhost:$PORT/v1/getExample", port), "GET")
	apitesting.HttpGetCompareBody(t, setHttpPort("http://localhost:$PORT/swagger.json", port), "futonredemptionGetExampleResponse")

	grpcInsecureTrial(assert, setGrpcPort("localhost:$PORT", port))

	server.StopServing()
	assert.NotNil(server)
}

func TestExampleServerSecure(t *testing.T) {
	assert := assert.New(t)
	port := nextPort()
	server := SetupServer(TEST_CERT_BUNDLE, false, true, port)

	certPool, err := getCertPoolForTest()
	assert.Nil(err)

	err = server.BeginServing()
	assert.Nil(err)
	err = server.WaitForServing()
	assert.Nil(err)
	apitesting.HttpsGetCompareBody(t, setHttpsPort("https://localhost:$PORT/swagger.json", port), certPool, "futonredemptionGetExampleResponse")

	grpcTlsTrial(assert, setGrpcTlsPort("localhost:$PORT", port), certPool)
	grpcTlsTrial(assert, setHttpsPort("localhost:$PORT", port), certPool)

	apitesting.HttpsGetCompareBody(t, setHttpsPort("https://localhost:$PORT/v1/getExample", port), certPool, "GET")
	server.StopServing()
	assert.NotNil(server)
}

func grpcInsecureTrial(assert *assert.Assertions, serverName string) {
	// Set up a connection to the server.
	conn, err := grpc.Dial(serverName, grpc.WithInsecure())
	assert.Nil(err)
	defer conn.Close()
	c := pb.NewExampleServiceClient(conn)
	r, err := c.PostExample(context.Background(), &pb.PostExampleRequest{Val: "val"})
	assert.Nil(err)
	assert.NotNil(r)
	if r != nil {
		assert.Equal(r.Val, "POST val")
	}
}

func grpcTlsTrial(assert *assert.Assertions, serverName string, certPool *x509.CertPool) {
	tlsClientConfig := &tls.Config{
		ServerName: serverName,
		RootCAs:    certPool,
	}
	dcreds := credentials.NewTLS(tlsClientConfig)
	dialOpts := []grpc.DialOption{grpc.WithTransportCredentials(dcreds)}

	tlsConn, err := grpc.Dial(serverName, dialOpts...)
	assert.Nil(err)
	defer tlsConn.Close()
	tlsGrpcClient := pb.NewExampleServiceClient(tlsConn)
	r, err := tlsGrpcClient.PostExample(context.Background(), &pb.PostExampleRequest{Val: "val"})
	assert.Nil(err)
	assert.NotNil(r)
	if r != nil {
		assert.Equal(r.Val, "POST val")
	}
}
