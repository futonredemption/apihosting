package example

import (
	"bitbucket.org/futonredemption/apihosting/apitesting"
	"crypto/x509"
	"fmt"
	"strconv"
	"strings"
)

var TEST_CERT_BUNDLE = &serverCertBundle{
	rootCaPublicPem:  []byte(apitesting.ROOT_PUB_PEM),
	rootCaPrivatePem: []byte(apitesting.ROOT_PRIV_PEM),
	serverPublicPem:  []byte(apitesting.SERVER_PUB_PEM),
	serverPrivatePem: []byte(apitesting.SERVER_PRIV_PEM),
}

func getCertPoolForTest() (*x509.CertPool, error) {
	caCerts := x509.NewCertPool()
	caCertPem := apitesting.ROOT_PUB_PEM
	ok := caCerts.AppendCertsFromPEM([]byte(caCertPem))
	if !ok {
		return nil, fmt.Errorf("Cannot append PEM: %s to certificate pool.", caCertPem)
	}
	return caCerts, nil
}

var currentPort int = 11000

func nextPort() int {
	port := currentPort
	currentPort += 10
	return port
}

func setHttpPort(url string, port int) string {
	return setPort(url, port)
}

func setGrpcPort(url string, port int) string {
	return setPort(url, port+1)
}

func setHttpsPort(url string, port int) string {
	return setPort(url, port+2)
}

func setGrpcTlsPort(url string, port int) string {
	return setPort(url, port+3)
}

func setPort(url string, port int) string {
	return strings.Replace(url, "$PORT", strconv.Itoa(port), 1)
}
