package main

import (
	"bitbucket.org/futonredemption/apihosting/example"
	"log"
)

func main() {
	server := example.SetupServer(example.TEST_CERT_BUNDLE, true, true, 10000)
	err := server.ServeAndWaitForever()
	if err != nil {
		log.Printf("Error: %v", err)
	}
}
