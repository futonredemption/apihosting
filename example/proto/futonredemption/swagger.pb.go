package futonredemption

const (
	example_swagger_json = `{
  "swagger": "2.0",
  "info": {
    "title": "futonredemption/example.proto",
    "version": "version not set"
  },
  "schemes": [
    "http",
    "https"
  ],
  "consumes": [
    "application/json"
  ],
  "produces": [
    "application/json"
  ],
  "paths": {
    "/v1/getExample": {
      "get": {
        "operationId": "GetExample",
        "responses": {
          "200": {
            "description": "",
            "schema": {
              "$ref": "#/definitions/futonredemptionGetExampleResponse"
            }
          }
        },
        "tags": [
          "ExampleService"
        ]
      }
    },
    "/v1/postExample": {
      "post": {
        "operationId": "PostExample",
        "responses": {
          "200": {
            "description": "",
            "schema": {
              "$ref": "#/definitions/futonredemptionPostExampleResponse"
            }
          }
        },
        "parameters": [
          {
            "name": "body",
            "in": "body",
            "required": true,
            "schema": {
              "$ref": "#/definitions/futonredemptionPostExampleRequest"
            }
          }
        ],
        "tags": [
          "ExampleService"
        ]
      }
    }
  },
  "definitions": {
    "futonredemptionGetExampleRequest": {
      "type": "object",
      "properties": {
        "val": {
          "type": "string",
          "format": "string"
        }
      }
    },
    "futonredemptionGetExampleResponse": {
      "type": "object",
      "properties": {
        "val": {
          "type": "string",
          "format": "string"
        }
      }
    },
    "futonredemptionPostExampleRequest": {
      "type": "object",
      "properties": {
        "val": {
          "type": "string",
          "format": "string"
        }
      }
    },
    "futonredemptionPostExampleResponse": {
      "type": "object",
      "properties": {
        "val": {
          "type": "string",
          "format": "string"
        }
      }
    }
  }
}
`
)
