package main

import (
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
)

var directory = flag.String("directory", ".", "The directory on the local filesystem to serve.")
var packageName = flag.String("package", "futonredemption", "Package name of the go output.")

// Reads all .json files in the current folder
// and encodes them as strings literals in textfiles.go
func main() {
	flag.Parse()
	fs, _ := ioutil.ReadDir(*directory)
	swaggerPbGo := filepath.Join(*directory, "swagger.pb.go")
	out, err := os.Create(swaggerPbGo)
	if err != nil {
		fmt.Printf("ERROR: %s", err)
	}
	defer out.Close()
	header := "package " + *packageName + " \n\nconst (\n"
	out.Write([]byte(header))
	for _, f := range fs {
		if strings.HasSuffix(f.Name(), ".swagger.json") {
			name := strings.TrimSuffix(f.Name(), ".swagger.json") + "_swagger_json"
			swaggerPbGoIn, err := filepath.Abs(filepath.Join(*directory, f.Name()))
			if err != nil {
				fmt.Printf("ERROR: %s", err)
			}
			out.Write([]byte(name + " = `"))
			f, err := os.Open(swaggerPbGoIn)
			if err != nil {
				fmt.Printf("ERROR: %s", err)
			}
			defer f.Close()
			io.Copy(out, f)
			out.Write([]byte("`\n"))
		}
	}
	out.Write([]byte(")\n"))
}
