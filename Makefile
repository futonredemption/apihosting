GO := @go
GOGET := go get -u -d
GOGETBUILD := go get -u
SOURCE_DIRS=$(shell go list ./... | grep -v '/vendor/')
export PATH := $(PATH):/usr/local/go/bin:/usr/go/bin

build: apiserving/web/bindata_assetfs.go

check: test

lint:
	$(GO) fmt ${SOURCE_DIRS}
	$(GO) vet ${SOURCE_DIRS}

apiserving/web/bindata_assetfs.go:
	rm -f apiserving/web/bindata_assetfs.go
	cd apiserving/web; go-bindata-assetfs -pkg web data/...

customtest:
	$(GO) test -cover ${SOURCE_DIRS} -race -count=100 -run TestInsecureDebug

test: apiserving/web/bindata_assetfs.go
	$(GO) test ${SOURCE_DIRS} -race
	
cover:
	$(GO) test ${SOURCE_DIRS} -cover

test-5:
	$(GO) test -count 5 ${SOURCE_DIRS} -race
	
coverage.txt: apiserving/web/bindata_assetfs.go
	$(GO) test -coverprofile=coverage.txt -covermode count ${SOURCE_DIRS}

bench: benchmark

benchmark:
	$(GO) test -cover -benchmem -bench=. ${SOURCE_DIRS}

full-test:
	$(GO) test -cover -coverprofile=$(PWD)/coverage.test.txt -blockprofile=$(PWD)/blockprofile.test.txt -cpuprofile=$(PWD)/cpuprofile.test.txt -memprofile=$(PWD)/memprofile.test.txt -trace=$(PWD)/trace.test.txt -race -covermode count ${SOURCE_DIRS}

clean:
	@rm -f *.test.txt coverage.txt apiserving/web/bindata_assetfs.go coverage.txt

deps:
	# gRPC
	$(GOGET) google.golang.org/grpc/...
	$(GOGETBUILD) github.com/golang/protobuf/proto
	$(GOGETBUILD) github.com/golang/protobuf/protoc-gen-go
	$(GOGETBUILD) github.com/grpc-ecosystem/grpc-gateway/...
	
	# Prometheus
	$(GOGET) github.com/prometheus/client_golang/...
	$(GOGET) github.com/grpc-ecosystem/go-grpc-prometheus/...
	
	# Extras
	$(GOGET) github.com/rs/cors
	
	# Resources
	$(GOGETBUILD) github.com/jteeuwen/go-bindata/...
	$(GOGETBUILD) github.com/elazarl/go-bindata-assetfs/...
	
	# Testing
	$(GOGETBUILD) github.com/t-yuki/gocover-cobertura
	$(GOGET) github.com/stretchr/testify/...

tools:
	$(GOGETBUILD) golang.org/x/tools/cmd/gorename
	$(GOGETBUILD) github.com/golang/lint/golint
	$(GOGETBUILD) github.com/nsf/gocode
	$(GOGETBUILD) github.com/rogpeppe/godef
	$(GOGETBUILD) github.com/lukehoban/go-outline
	$(GOGETBUILD) github.com/newhook/go-symbols
	$(GOGETBUILD) github.com/sqs/goreturns

.PHONY : all lint check customtest test test-50 cover bench benchmark full-test clean deps tools
