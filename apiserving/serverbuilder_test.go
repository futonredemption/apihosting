package apiserving

import (
	"testing"
)

func TestGrpcHttpApiServerBuilderConstructor(t *testing.T) {
	builder := newGrpcHttpApiServerBuilder()
	if builder == nil {
		t.Errorf("grpcHttpApiServerBuilder = %v", builder)
	}
}
