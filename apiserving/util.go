package apiserving

import (
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"github.com/prometheus/client_golang/prometheus"
	"google.golang.org/grpc"
	"io/ioutil"
	"net"
	"net/http"
	"strings"
	"time"
)

func portAddress(port int) string {
	return fmt.Sprintf(":%d", port)
}

func newTCPListener(address string) (net.Listener, error) {
	return net.Listen("tcp", address)
}

func grpcHandlerFunc(grpcServer *grpc.Server, otherHandler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// TODO(tamird): point to merged gRPC code rather than a PR.
		// This is a partial recreation of gRPC's internal checks https://github.com/grpc/grpc-go/pull/514/files#diff-95e9a25b738459a2d3030e1e6fa2a718R61
		if r.ProtoMajor == 2 && strings.Contains(r.Header.Get("Content-Type"), "application/grpc") {
			grpcServer.ServeHTTP(w, r)
		} else {
			otherHandler.ServeHTTP(w, r)
		}
	})
}

func newTLSCertPoolFromFile(caCertPemPath string) (*x509.CertPool, error) {
	caCertPemData, err := ioutil.ReadFile(caCertPemPath)
	if err != nil {
		return nil, err
	}
	return newTLSCertPoolFromBytes(caCertPemData)
}

func newTLSCertPoolFromBytes(caCertPem []byte) (*x509.CertPool, error) {
	caCerts := x509.NewCertPool()
	ok := caCerts.AppendCertsFromPEM(caCertPem)
	if !ok {
		return nil, fmt.Errorf("Cannot append PEM: %s to certificate pool.", caCertPem)
	}
	return caCerts, nil
}

func newTLSKeyPairFromPEMFiles(certPemPath, privPemPath string) (*tls.Certificate, error) {
	pubPem, err := ioutil.ReadFile(certPemPath)
	if err != nil {
		return nil, err
	}
	privPem, err := ioutil.ReadFile(privPemPath)
	if err != nil {
		return nil, err
	}
	return newTLSKeyPairFromPEMBytes(pubPem, privPem)
}

func newTLSKeyPairFromPEMBytes(pubPem, privPem []byte) (*tls.Certificate, error) {
	pair, err := tls.X509KeyPair(pubPem, privPem)
	if err != nil {
		return nil, err
	}
	return &pair, nil
}

func newTLSClientConfig(address string, certPool *x509.CertPool) *tls.Config {
	return &tls.Config{
		ServerName: address,
		RootCAs:    certPool,
	}
}

func newTLSServerConfig(serverKeyPair *tls.Certificate) *tls.Config {
	return &tls.Config{
		Certificates: []tls.Certificate{*serverKeyPair},
		NextProtos:   []string{"h2"}, // https://github.com/grpc-ecosystem/grpc-gateway/issues/220
	}
}

func addHandler(mux *http.ServeMux, path string, handler http.Handler, prom bool) {
	if prom {
		mux.Handle(path, prometheus.InstrumentHandler(path, handler))
	} else {
		mux.Handle(path, handler)
	}
}

func addHandlerFunc(mux *http.ServeMux, path string, handlerFunc http.HandlerFunc, prom bool) {
	if prom {
		mux.HandleFunc(path, prometheus.InstrumentHandlerFunc(path, handlerFunc))
	} else {
		mux.HandleFunc(path, handlerFunc)
	}
}

func enablePrometheus(mux *http.ServeMux) {
	mux.Handle("/metrics", prometheus.Handler())
}

func sleep(ms time.Duration) {
	time.Sleep(ms * time.Millisecond)
}

func sleepForWarmup() {
	sleep(100)
}

func sleepForLongWarmup() {
	sleep(1000)
}
