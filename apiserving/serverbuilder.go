package apiserving

import (
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"net/http"
)

type httpHandlerParams struct {
	pattern     string
	handler     http.Handler
	handlerFunc http.HandlerFunc
}

type grpcHttpApiServerBuilder struct {
	httpPort                int
	httpsPort               int
	insecureGrpcPort        int
	tlsGrpcPort             int
	rootCaPemPath           string
	serverPublicKeyPemPath  string
	serverPrivateKeyPemPath string
	rootCaPem               []byte
	serverPublicKeyPem      []byte
	serverPrivateKeyPem     []byte
	debugInsecure           bool
	debugSecure             bool
	enablePrometheus        bool
	swaggerHttpHandler      func(http.ResponseWriter, *http.Request)
	insecureRegs            []GrpcRegistrationFunc
	secureRegs              []GrpcRegistrationFunc
	insecureRestRegs        []GrpcRestRegistrationFunc
	secureRestRegs          []GrpcRestRegistrationFunc
	insecureHttpHandlers    []httpHandlerParams
	secureHttpHandlers      []httpHandlerParams
}

func (this *grpcHttpApiServerBuilder) getTlsPublicRootCert() (*x509.CertPool, error) {
	if len(this.rootCaPem) > 0 {
		return newTLSCertPoolFromBytes(this.rootCaPem)
	} else if this.rootCaPemPath != "" {
		return newTLSCertPoolFromFile(this.rootCaPemPath)
	} else {
		return nil, fmt.Errorf("No Root CA was specified.")
	}
}

func (this *grpcHttpApiServerBuilder) getTlsKeyPair() (*tls.Certificate, error) {
	if len(this.serverPublicKeyPem) > 0 {
		return newTLSKeyPairFromPEMBytes(this.serverPublicKeyPem, this.serverPrivateKeyPem)
	} else if this.serverPublicKeyPemPath != "" {
		return newTLSKeyPairFromPEMFiles(this.serverPublicKeyPemPath, this.serverPrivateKeyPemPath)
	} else {
		return nil, fmt.Errorf("No Server Pub-Private Key was specified.")
	}
}

func newGrpcHttpApiServerBuilder() *grpcHttpApiServerBuilder {
	return &grpcHttpApiServerBuilder{
		insecureRegs:         make([]GrpcRegistrationFunc, 0),
		secureRegs:           make([]GrpcRegistrationFunc, 0),
		insecureRestRegs:     make([]GrpcRestRegistrationFunc, 0),
		secureRestRegs:       make([]GrpcRestRegistrationFunc, 0),
		insecureHttpHandlers: make([]httpHandlerParams, 0),
		secureHttpHandlers:   make([]httpHandlerParams, 0),
	}
}
