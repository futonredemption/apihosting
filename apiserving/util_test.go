package apiserving

import (
	"testing"
)

func TestPortAddress(t *testing.T) {
	actual := portAddress(80)
	expected := ":80"
	if actual != expected {
		t.Errorf("portStr = %s, expected %s", actual, expected)
	}
}

func TestNewTCPAddress(t *testing.T) {
	address := "localhost:8000"
	actual, err := newTCPListener(address)
	if err != nil {
		t.Errorf("Error: %s", err)
	}
	if actual == nil {
		t.Errorf("TCP Listener is: %s", actual)
	}
}
