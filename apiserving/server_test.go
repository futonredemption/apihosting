package apiserving

import (
	"testing"
)

func TestConstructor(t *testing.T) {
	New().
		SetTlsGrpcPort(10001).
		SetHttpsPort(10000).
		SetGrpcPort(8081).
		SetHttpPort(8080).
		SetCertificatesFromFile("server.cert.pem", "server.key.pem").
		SetRootCertificateFromFile("root.cert.pem").
		AddHandler("/hi", httpHandlerFunc).
		EnableMetrics().
		EnableDebug()
}

func TestHttpOnly_PreventStart(t *testing.T) {
	server := New().
		SetHttpPort(21000).
		AddHandler("/hi", httpHandlerFunc)
	err := server.BeginServing()
	if err == nil {
		t.Errorf("BeginServing() = %v", err)
	}
}

func TestHttpsOnly_PreventStart(t *testing.T) {
	server := New().
		SetHttpsPort(21000).
		AddHandler("/hi", httpHandlerFunc)
	err := server.BeginServing()
	if err == nil {
		t.Errorf("BeginServing() = %v", err)
	}
}

func TestHttpAndHttpsOnly_PreventStart(t *testing.T) {
	server := New().
		SetHttpPort(21000).
		SetHttpsPort(21001).
		AddHandler("/hi", httpHandlerFunc)
	err := server.BeginServing()
	if err == nil {
		t.Errorf("BeginServing() = %v", err)
	}
}

func TestInsecure(t *testing.T) {
	httpPort := nextPort()
	grpcPort := nextPort()
	server := New().
		SetHttpPort(httpPort).
		SetGrpcPort(grpcPort).
		AddHandler("/hi", httpHandlerFunc).
		EnableMetrics()
	err := server.BeginServing()
	if err != nil {
		t.Errorf("BeginServing() = %v", err)
	}
	err = server.WaitForServing()
	if err != nil {
		t.Errorf("WaitForServing() = %v", err)
	}
	httpGetCompareBody(t, setPort("http://localhost:$PORT/hi", httpPort), "Hi")

	err = server.StopServing()
	if err != nil {
		t.Errorf("StopServing() = %v", err)
	}
}

func TestInsecureDebug(t *testing.T) {
	httpPort := nextPort()
	grpcPort := nextPort()
	server := New().
		SetHttpPort(httpPort).
		SetGrpcPort(grpcPort).
		EnableDebug().
		EnableMetrics()
	err := server.BeginServing()
	if err != nil {
		t.Errorf("BeginServing() = %v", err)
	}

	err = server.WaitForServing()
	if err != nil {
		t.Errorf("WaitForServing() = %v", err)
	}
	httpGetCompareBody(t, setPort("http://localhost:$PORT/debug/pprof/", httpPort), "full goroutine stack dump")
	httpGetCompareBody(t, setPort("http://localhost:$PORT/debug/pprof/heap", httpPort), "runtime.MemStats", "heap profile")
	httpGetCompareBody(t, setPort("http://localhost:$PORT/debug/pprof/cmdline", httpPort), "apiserving")
	httpGetCompareBody(t, setPort("http://localhost:$PORT/metrics", httpPort), "http_requests_total")

	httpGetCompareBody(t, setPort("http://localhost:$PORT/swagger/index.html", httpPort), "docs.html")
	httpGetCompareBody(t, setPort("http://localhost:$PORT/swagger/index.html", httpPort), "API Browser")
	httpGetCompareBody(t, setPort("http://localhost:$PORT/swagger/docs.html", httpPort), "swagger.json")
	httpGetCompareBody(t, setPort("http://localhost:$PORT/swagger/ui/index.html", httpPort), "Swagger UI")
	httpGetCompareBody(t, setPort("http://localhost:$PORT/swagger/ui/swagger-ui.js", httpPort), "function")

	err = server.StopServing()
	if err != nil {
		t.Errorf("StopServing() = %v", err)
	}
}

func TestSecure(t *testing.T) {
	httpsPort := nextPort()
	grpcTlsPort := nextPort()
	server := New().
		SetHttpsPort(httpsPort).
		SetTlsGrpcPort(grpcTlsPort).
		SetRootCertificateFromPEM(TEST_ROOT_PUB_PEM).
		SetCertificatesFromPEM(TEST_PUB_PEM, TEST_PRIV_PEM).
		AddHandler("/hi", httpHandlerFunc)

	err := server.BeginServing()
	if err != nil {
		t.Errorf("BeginServing() = %v", err)
	}
	err = server.WaitForServing()
	if err != nil {
		t.Errorf("WaitForServing() = %v", err)
	}

	cServer, ok := server.(*grpcHttpApiServer)
	if !ok {
		t.Errorf("%v is not *grpcHttpApiServer", server)
	}
	httpsGetCompareBody(t, setPort("https://localhost:$PORT/hi", httpsPort), cServer.secure.certPool, "Hi")

	err = server.StopServing()
	if err != nil {
		t.Errorf("StopServing() = %v", err)
	}
}

func TestSecureDebug(t *testing.T) {
	httpsPort := nextPort()
	grpcTlsPort := nextPort()
	server := New().
		SetHttpsPort(httpsPort).
		SetTlsGrpcPort(grpcTlsPort).
		SetRootCertificateFromPEM(TEST_ROOT_PUB_PEM).
		SetCertificatesFromPEM(TEST_PUB_PEM, TEST_PRIV_PEM).
		EnableDebug().
		EnableMetrics()
	err := server.BeginServing()
	if err != nil {
		t.Errorf("BeginServing() = %v", err)
	}
	err = server.WaitForServing()
	if err != nil {
		t.Errorf("WaitForServing() = %v", err)
	}

	cServer, ok := server.(*grpcHttpApiServer)
	if !ok {
		t.Errorf("%v is not *grpcHttpApiServer", server)
	}

	httpsGetCompareBody(t, setPort("https://localhost:$PORT/debug/pprof/", httpsPort), cServer.secure.certPool, "full goroutine stack dump")
	httpsGetCompareBody(t, setPort("https://localhost:$PORT/debug/pprof/heap", httpsPort), cServer.secure.certPool, "runtime.MemStats", "heap profile")
	httpsGetCompareBody(t, setPort("https://localhost:$PORT/debug/pprof/cmdline", httpsPort), cServer.secure.certPool, "apiserving")
	httpsGetCompareBody(t, setPort("https://localhost:$PORT/metrics", httpsPort), cServer.secure.certPool, "http_requests_total")

	err = server.StopServing()
	if err != nil {
		t.Errorf("StopServing() = %v", err)
	}
}
