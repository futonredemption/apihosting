package apiserving

import (
	"bitbucket.org/futonredemption/apihosting/apiserving/web"
	"fmt"
	"golang.org/x/net/trace"
	"net/http"
	"net/http/pprof"
)

const debug_index = `<html>
<head>
<title>Server Debugging</title>
</head>
<body>
<a href="/index.html">Server Home</a>
<br /><h2>Debug Pages</h2>
<a href="/debug/health">Health Check</a>
<br /><a href="/debug/pprof/">Pprof</a>
<br /><a href="/debug/pprof/cmdline">Command Line</a>
<br /><a href="/debug/pprof/profile">Profiler</a>
<br /><a href="/debug/pprof/symbol">Symbols</a>
<br /><a href="/debug/pprof/trace">Trace</a>
<br /><a href="/debug/requests">Requests</a>
<br /><a href="/debug/events">Events</a>
</body>
</html>`

func addHealthCheck(mux *http.ServeMux, enablePrometheus bool) {
	healthFunc := func(w http.ResponseWriter, req *http.Request) {
		fmt.Fprintf(w, "OK")
	}
	addHandlerFunc(mux, "/debug/health", http.HandlerFunc(healthFunc), enablePrometheus)
}

func addDebugIndex(mux *http.ServeMux, enablePrometheus bool) {

}

func addPprof(mux *http.ServeMux, enablePrometheus bool) {
	trace.AuthRequest = func(req *http.Request) (any, sensitive bool) {
		return true, true
	}
	debugIndexFunc := func(w http.ResponseWriter, req *http.Request) {
		fmt.Fprintf(w, debug_index)
	}
	addHandlerFunc(mux, "/debug", http.HandlerFunc(debugIndexFunc), enablePrometheus)
	addHandlerFunc(mux, "/debug/index.html", http.HandlerFunc(debugIndexFunc), enablePrometheus)
	addHandlerFunc(mux, "/debug/pprof/", http.HandlerFunc(pprof.Index), enablePrometheus)
	addHandlerFunc(mux, "/debug/pprof/cmdline", http.HandlerFunc(pprof.Cmdline), enablePrometheus)
	addHandlerFunc(mux, "/debug/pprof/profile", http.HandlerFunc(pprof.Profile), enablePrometheus)
	addHandlerFunc(mux, "/debug/pprof/symbol", http.HandlerFunc(pprof.Symbol), enablePrometheus)
	addHandlerFunc(mux, "/debug/pprof/trace", http.HandlerFunc(pprof.Trace), enablePrometheus)
}

func addSwaggerUi(mux *http.ServeMux, enablePrometheus bool) {
	addHandler(mux, "/swagger/", http.FileServer(web.AssetFS()), false)
}
