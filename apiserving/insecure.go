package apiserving

import (
	"fmt"
	"github.com/grpc-ecosystem/go-grpc-prometheus"
	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"github.com/rs/cors"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
	"log"
	"net"
	"net/http"
	"sync"
)

type insecureServer struct {
	grpcServer          *grpc.Server
	grpcEndpointAddress string
	grpcTcpListener     net.Listener
	serveGrpcFunc       func() error
	grpcAwaiter         chan error
	grpcMutex           sync.Mutex

	httpRestCtx             context.Context
	httpRootMux             *http.ServeMux
	httpRestMux             *runtime.ServeMux
	httpRestEndpointAddress string
	httpServer              *http.Server
	httpTcpListener         net.Listener
	serveHttpRestFunc       func() error
	httpAwaiter             chan error
	httpTcpMutex            sync.Mutex

	startWaitGroup sync.WaitGroup
}

func (this *insecureServer) setGrpcListener(lis net.Listener) {
	this.grpcMutex.Lock()
	defer this.grpcMutex.Unlock()
	this.grpcTcpListener = lis
}

func (this *insecureServer) closeGrpcListener() error {
	var err error
	this.grpcMutex.Lock()
	defer this.grpcMutex.Unlock()
	if this.grpcTcpListener != nil {
		err = this.grpcTcpListener.Close()
	}
	return err
}

func (this *insecureServer) setHttpListener(lis net.Listener) {
	this.httpTcpMutex.Lock()
	defer this.httpTcpMutex.Unlock()
	this.httpTcpListener = lis
}

func (this *insecureServer) closeHttpListener() error {
	var err error
	this.httpTcpMutex.Lock()
	defer this.httpTcpMutex.Unlock()
	if this.httpTcpListener != nil {
		err = this.httpTcpListener.Close()
	}
	return err
}

func (this *insecureServer) beginServing() error {
	this.grpcAwaiter = make(chan error)
	this.startWaitGroup.Add(1)
	go func() {
		err := this.serveGrpcFunc()
		this.grpcAwaiter <- err
		if err != nil {
			log.Printf("Insecure gRPC Server Error: %v", err)
		}
	}()
	if this.serveHttpRestFunc != nil {
		this.startWaitGroup.Add(1)
		this.httpAwaiter = make(chan error)
		go func() {
			err := this.serveHttpRestFunc()
			this.httpAwaiter <- err
			if err != nil {
				log.Printf("Insecure gRPC REST Gateway Server Error: %v", err)
			}
		}()
	}
	return nil
}

func (this *insecureServer) waitForServing() {
	this.startWaitGroup.Wait()
	sleepForWarmup()
}

func (this *insecureServer) stopServing() error {
	err := this.closeHttpListener()
	if err != nil {
		return err
	}
	err = this.closeGrpcListener()
	if err != nil {
		return err
	}
	this.waitForTermination()
	return err
}

func (this *insecureServer) waitForTermination() error {
	var err error
	if this.httpAwaiter != nil {
		err = <-this.httpAwaiter
		close(this.httpAwaiter)
		this.httpAwaiter = nil
	}
	if this.grpcAwaiter != nil {
		err = <-this.grpcAwaiter
		close(this.grpcAwaiter)
		this.grpcAwaiter = nil
	}
	return err
}

func buildInsecure(builder *grpcHttpApiServerBuilder) (*insecureServer, error) {
	server := &insecureServer{}
	if builder.httpPort > 0 || builder.insecureGrpcPort > 0 {
		if builder.httpPort > 0 && builder.insecureGrpcPort == 0 {
			return nil, fmt.Errorf("Cannot host insecure API over HTTP via port %d without an insecure gRPC port", builder.httpPort)
		}
		grpcStarting := make(chan bool)
		server.grpcEndpointAddress = portAddress(builder.insecureGrpcPort)
		server.grpcServer = newInsecureGrpc(builder.enablePrometheus)
		for _, handlerReg := range builder.insecureRegs {
			handlerReg(server.grpcServer)
		}

		grpcLis, err := newTCPListener(server.grpcEndpointAddress)
		if err != nil {
			return nil, err
		}
		server.setGrpcListener(grpcLis)
		server.serveGrpcFunc = func() error {
			grpcStarting <- true
			server.startWaitGroup.Done()
			return server.grpcServer.Serve(server.grpcTcpListener)
		}
		if builder.httpPort > 0 {
			server.httpRestEndpointAddress = portAddress(builder.httpPort)
			server.httpRestCtx = context.Background()
			server.httpRootMux = http.NewServeMux()
			server.httpRestMux = runtime.NewServeMux()
			if builder.swaggerHttpHandler != nil {
				addHandlerFunc(server.httpRootMux, "/swagger.json", builder.swaggerHttpHandler, builder.enablePrometheus)
			}
			addHandler(server.httpRootMux, "/", server.httpRestMux, builder.enablePrometheus)
			if builder.debugInsecure {
				addPprof(server.httpRootMux, builder.enablePrometheus)
				addSwaggerUi(server.httpRootMux, builder.enablePrometheus)
			}

			if builder.enablePrometheus {
				enablePrometheus(server.httpRootMux)
			}
			addHealthCheck(server.httpRootMux, builder.enablePrometheus)

			server.httpServer = &http.Server{
				Addr:    server.httpRestEndpointAddress,
				Handler: cors.Default().Handler(grpcHandlerFunc(server.grpcServer, server.httpRootMux)),
			}
			tcpLis, err := newTCPListener(server.httpRestEndpointAddress)
			if err != nil {
				return nil, err
			}
			server.setHttpListener(tcpLis)
			server.serveHttpRestFunc = func() error {
				reached := false
				defer func() {
					if !reached {
						server.startWaitGroup.Done()
					}
				}()
				<-grpcStarting
				close(grpcStarting)

				// HACK: Give a bit more time for gRPC to become available.
				sleepForWarmup()
				opts := []grpc.DialOption{grpc.WithInsecure()}

				for _, handlerReg := range builder.insecureRestRegs {
					err := handlerReg(server.httpRestCtx, server.httpRestMux, server.grpcEndpointAddress, opts)
					if err != nil {
						return err
					}
				}
				for _, handlerReg := range builder.insecureHttpHandlers {
					if handlerReg.handlerFunc != nil {
						addHandlerFunc(server.httpRootMux, handlerReg.pattern, handlerReg.handlerFunc, builder.enablePrometheus)
					} else {
						addHandler(server.httpRootMux, handlerReg.pattern, handlerReg.handler, builder.enablePrometheus)
					}
				}
				reached = true
				server.startWaitGroup.Done()
				return server.httpServer.Serve(server.httpTcpListener)
			}
		}
		return server, err
	} else {
		return nil, nil
	}
}

func newInsecureGrpc(enablePrometheus bool) *grpc.Server {
	opts := []grpc.ServerOption{}
	if enablePrometheus {
		opts = append(opts, grpc.StreamInterceptor(grpc_prometheus.StreamServerInterceptor))
		opts = append(opts, grpc.UnaryInterceptor(grpc_prometheus.UnaryServerInterceptor))
		grpc_prometheus.EnableHandlingTimeHistogram()
	}
	s := grpc.NewServer(opts...)
	reflection.Register(s)
	return s
}
