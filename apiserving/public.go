package apiserving

import (
	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"net/http"
)

// GrpcRegistrationFunc is a callback method to register gRPC services to the API server.
type GrpcRegistrationFunc func(grpcServer *grpc.Server)

// GrpcRestRegistrationFunc is a callback to register gRPC REST Gateway services to the API server.
type GrpcRestRegistrationFunc func(ctx context.Context, mux *runtime.ServeMux, address string, opts []grpc.DialOption) error

// A server capable of handling HTTP REST and gRPC requests.
type ApiServer interface {
	SetHttpPort(port int) ApiServer
	SetHttpsPort(port int) ApiServer
	SetGrpcPort(port int) ApiServer
	SetTlsGrpcPort(port int) ApiServer
	SetRootCertificateFromFile(rootCaPemPath string) ApiServer
	SetCertificatesFromFile(serverPublicKeyPemPath, serverPrivateKeyPemPath string) ApiServer
	SetRootCertificateFromPEM(rootCaPem []byte) ApiServer
	SetCertificatesFromPEM(serverPublicKeyPem, serverPrivateKeyPem []byte) ApiServer
	Add(service GrpcRegistrationFunc) ApiServer
	AddInsecure(service GrpcRegistrationFunc) ApiServer
	AddSecure(service GrpcRegistrationFunc) ApiServer
	AddRest(service GrpcRestRegistrationFunc) ApiServer
	AddRestInsecure(service GrpcRestRegistrationFunc) ApiServer
	AddRestSecure(service GrpcRestRegistrationFunc) ApiServer
	AddHandler(pattern string, handler http.HandlerFunc) ApiServer
	AddHandlerInsecure(pattern string, handler http.HandlerFunc) ApiServer
	AddHandlerSecure(pattern string, handler http.HandlerFunc) ApiServer

	AddHttpHandler(pattern string, handler http.Handler) ApiServer
	AddHttpHandlerInsecure(pattern string, handler http.Handler) ApiServer
	AddHttpHandlerSecure(pattern string, handler http.Handler) ApiServer

	EnableDebug() ApiServer
	EnableDebugInsecure() ApiServer
	EnableDebugSecure() ApiServer
	SetSwaggerString(swaggerString string) ApiServer
	EnableMetrics() ApiServer
	BeginServing() error
	BeginServingInsecure() error
	BeginServingSecure() error
	WaitForServing() error
	WaitForServingInsecure() error
	WaitForServingSecure() error
	StopServing() error
	StopServingInsecure() error
	StopServingSecure() error
	ServeAndWaitForever() error
	Wait() error
}

// Creates a new API server.
func New() ApiServer {
	return newGrpcHttpApiServer()
}
