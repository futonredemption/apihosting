package apiserving

import (
	"crypto/tls"
	"crypto/x509"
	"github.com/grpc-ecosystem/go-grpc-prometheus"
	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"github.com/rs/cors"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/reflection"
	"log"
	"net"
	"net/http"
	"sync"
)

type secureServer struct {
	grpcServer          *grpc.Server
	grpcEndpointAddress string
	serveGrpcFunc       func() error
	grpcAwaiter         chan error
	grpcTcpListener     net.Listener
	grpcMutex           sync.Mutex

	httpsRestCtx             context.Context
	httpsRestGrpcServer      *grpc.Server
	httpsRootMux             *http.ServeMux
	httpsRestMux             *runtime.ServeMux
	httpsRestEndpointAddress string
	httpsServer              *http.Server
	serveHttpsRestFunc       func() error
	httpsAwaiter             chan error
	httpsTcpListener         net.Listener
	httpsTcpMutex            sync.Mutex

	startWaitGroup sync.WaitGroup
	keyPair        *tls.Certificate
	certPool       *x509.CertPool
}

func (this *secureServer) setGrpcListener(lis net.Listener) {
	this.grpcMutex.Lock()
	defer this.grpcMutex.Unlock()
	this.grpcTcpListener = lis
}

func (this *secureServer) closeGrpcListener() error {
	var err error
	this.grpcMutex.Lock()
	defer this.grpcMutex.Unlock()
	if this.grpcTcpListener != nil {
		err = this.grpcTcpListener.Close()
	}
	return err
}

func (this *secureServer) setHttpsListener(lis net.Listener) {
	this.httpsTcpMutex.Lock()
	defer this.httpsTcpMutex.Unlock()
	this.httpsTcpListener = lis
}

func (this *secureServer) closeHttpsListener() error {
	var err error
	this.httpsTcpMutex.Lock()
	defer this.httpsTcpMutex.Unlock()
	if this.httpsTcpListener != nil {
		err = this.httpsTcpListener.Close()
	}
	return err
}

func (this *secureServer) beginServing() error {
	if this.serveGrpcFunc != nil {
		this.startWaitGroup.Add(1)
		this.grpcAwaiter = make(chan error)
		go func() {
			err := this.serveGrpcFunc()
			this.grpcAwaiter <- err
			if err != nil {
				log.Printf("Secure gRPC Server Error: %v", err)
			}
		}()
	}
	if this.serveHttpsRestFunc != nil {
		this.startWaitGroup.Add(1)
		this.httpsAwaiter = make(chan error)
		go func() {
			err := this.serveHttpsRestFunc()
			this.httpsAwaiter <- err
			if err != nil {
				log.Printf("Secure gRPC REST Gateway Server Error: %v", err)
			}
		}()
	}
	return nil
}

func (this *secureServer) waitForServing() {
	this.startWaitGroup.Wait()
	sleepForLongWarmup()
}

func (this *secureServer) stopServing() error {
	err := this.closeHttpsListener()
	if err != nil {
		return err
	}
	return this.closeGrpcListener()
}

func (this *secureServer) waitForTermination() error {
	var err error
	if this.httpsAwaiter != nil {
		err = <-this.httpsAwaiter
		close(this.httpsAwaiter)
		this.httpsAwaiter = nil
	}
	if this.grpcAwaiter != nil {
		err = <-this.grpcAwaiter
		close(this.grpcAwaiter)
		this.grpcAwaiter = nil
	}
	return err
}

func buildSecure(builder *grpcHttpApiServerBuilder) (*secureServer, error) {
	var err error
	if builder.httpsPort > 0 || builder.tlsGrpcPort > 0 {
		server := &secureServer{}
		if builder.httpsPort > 0 {
			server.certPool, err = builder.getTlsPublicRootCert()
			if err != nil {
				return nil, err
			}
		}
		server.keyPair, err = builder.getTlsKeyPair()
		if err != nil {
			return nil, err
		}
		server.grpcServer = newTLSGrpc(server.keyPair, builder.enablePrometheus)

		// Used by both HTTPS and TLS-gRPC
		for _, handlerReg := range builder.secureRegs {
			handlerReg(server.grpcServer)
		}

		if builder.tlsGrpcPort > 0 {
			server.grpcEndpointAddress = portAddress(builder.tlsGrpcPort)

			server.serveGrpcFunc = func() error {
				lis, err := newTCPListener(server.grpcEndpointAddress)
				if err != nil {
					return err
				}

				server.setGrpcListener(lis)
				server.startWaitGroup.Done()
				return server.grpcServer.Serve(server.grpcTcpListener)
			}
		}

		if builder.httpsPort > 0 {
			server.httpsRestEndpointAddress = portAddress(builder.httpsPort)
			server.httpsRestGrpcServer = newTLSClientGrpc(server.certPool, server.httpsRestEndpointAddress, builder.enablePrometheus)
			server.httpsRestCtx = context.Background()
			tlsClientConfig := newTLSClientConfig(server.httpsRestEndpointAddress, server.certPool)

			clientCreds := credentials.NewTLS(tlsClientConfig)
			dialOpts := []grpc.DialOption{grpc.WithTransportCredentials(clientCreds)}

			server.httpsRootMux = http.NewServeMux()
			server.httpsRestMux = runtime.NewServeMux()
			if builder.swaggerHttpHandler != nil {
				addHandlerFunc(server.httpsRootMux, "/swagger.json", builder.swaggerHttpHandler, builder.enablePrometheus)
			}
			addHandler(server.httpsRootMux, "/", server.httpsRestMux, builder.enablePrometheus)
			if builder.enablePrometheus {
				enablePrometheus(server.httpsRootMux)
			}
			if builder.debugSecure {
				addPprof(server.httpsRootMux, builder.enablePrometheus)
				addSwaggerUi(server.httpsRootMux, builder.enablePrometheus)
			}
			addHealthCheck(server.httpsRootMux, builder.enablePrometheus)

			server.httpsServer = &http.Server{
				Addr:      server.httpsRestEndpointAddress,
				Handler:   cors.Default().Handler(grpcHandlerFunc(server.httpsRestGrpcServer, server.httpsRootMux)),
				TLSConfig: newTLSServerConfig(server.keyPair),
			}

			// TODO: May have to move to deferred func below.
			for _, handlerReg := range builder.secureRestRegs {
				err := handlerReg(server.httpsRestCtx, server.httpsRestMux, server.httpsRestEndpointAddress, dialOpts)
				if err != nil {
					return nil, err
				}
			}

			for _, handlerReg := range builder.secureHttpHandlers {
				if handlerReg.handlerFunc != nil {
					addHandlerFunc(server.httpsRootMux, handlerReg.pattern, handlerReg.handlerFunc, builder.enablePrometheus)
				} else {
					addHandler(server.httpsRootMux, handlerReg.pattern, handlerReg.handler, builder.enablePrometheus)
				}
			}

			for _, handlerReg := range builder.secureRegs {
				handlerReg(server.httpsRestGrpcServer)
			}

			server.serveHttpsRestFunc = func() error {
				lis, err := newTCPListener(server.httpsRestEndpointAddress)
				if err != nil {
					return err
				}
				tcpLis := tls.NewListener(lis, server.httpsServer.TLSConfig)
				server.setHttpsListener(tcpLis)
				server.startWaitGroup.Done()
				return server.httpsServer.Serve(server.httpsTcpListener)
			}
		}
		return server, err
	} else {
		return nil, nil
	}
}

func newTLSGrpc(keyPair *tls.Certificate, enablePrometheus bool) *grpc.Server {
	creds := credentials.NewServerTLSFromCert(keyPair)
	opts := []grpc.ServerOption{grpc.Creds(creds)}

	if enablePrometheus {
		opts = append(opts, grpc.StreamInterceptor(grpc_prometheus.StreamServerInterceptor))
		opts = append(opts, grpc.UnaryInterceptor(grpc_prometheus.UnaryServerInterceptor))
	}
	s := grpc.NewServer(opts...)

	return s
}

func newTLSClientGrpc(certPool *x509.CertPool, serverName string, enablePrometheus bool) *grpc.Server {
	creds := credentials.NewClientTLSFromCert(certPool, serverName)
	opts := []grpc.ServerOption{grpc.Creds(creds)}

	if enablePrometheus {
		opts = append(opts, grpc.StreamInterceptor(grpc_prometheus.StreamServerInterceptor))
		opts = append(opts, grpc.UnaryInterceptor(grpc_prometheus.UnaryServerInterceptor))
		grpc_prometheus.EnableHandlingTimeHistogram()
	}
	s := grpc.NewServer(opts...)
	reflection.Register(s)
	return s
}
