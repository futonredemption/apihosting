package apiserving

import (
	"net"
	"net/http"
	"strings"
	"testing"
	"time"
)

func TestStartDebug(t *testing.T) {
	port := nextPort()
	addressPort := portAddress(port)
	serveErr := make(chan error)
	mux := newHttpTestMux()
	addPprof(mux, true)
	l, err := net.Listen("tcp", addressPort)
	if err != nil {
		t.Error(err)
	}
	s := newServer(mux, addressPort)
	go func() {
		serveErr <- s.Serve(l)
		close(serveErr)
	}()
	httpGetCompareBody(t, setPort("http://localhost:$PORT/debug/pprof/heap", port), "runtime.MemStats")
	l.Close()
	err = <-serveErr
	if err != nil {
		if !strings.Contains(err.Error(), "use of closed network") {
			t.Error(err)
		}
	} else {
		t.Errorf("Expected closed network connection, got %v", err)
	}
}

func TestHealthCheck(t *testing.T) {
	port := nextPort()
	addressPort := portAddress(port)
	serveErr := make(chan error)
	mux := newHttpTestMux()
	addHealthCheck(mux, true)
	l, err := net.Listen("tcp", addressPort)
	if err != nil {
		t.Error(err)
	}
	s := newServer(mux, addressPort)
	go func() {
		serveErr <- s.Serve(l)
	}()
	httpGetCompareBody(t, setPort("http://localhost:$PORT/debug/health", port), "OK")

	l.Close()
	err = <-serveErr
	if err != nil {
		if !strings.Contains(err.Error(), "use of closed network") {
			t.Error(err)
		}
	} else {
		t.Errorf("Expected closed network connection, got %v", err)
	}
}

func TestSwaggerUi(t *testing.T) {
	port := nextPort()
	addressPort := portAddress(port)
	serveErr := make(chan error)
	mux := newHttpTestMux()
	addSwaggerUi(mux, true)
	l, err := net.Listen("tcp", addressPort)
	if err != nil {
		t.Error(err)
	}
	s := newServer(mux, addressPort)
	go func() {
		serveErr <- s.Serve(l)
	}()
	httpGetCompareBody(t, setPort("http://localhost:$PORT/swagger/index.html", port), "docs.html")
	httpGetCompareBody(t, setPort("http://localhost:$PORT/swagger/index.html", port), "API Browser")
	httpGetCompareBody(t, setPort("http://localhost:$PORT/swagger/docs.html", port), "swagger.json")
	httpGetCompareBody(t, setPort("http://localhost:$PORT/swagger/ui/index.html", port), "Swagger UI")
	httpGetCompareBody(t, setPort("http://localhost:$PORT/swagger/ui/swagger-ui.js", port), "function")

	l.Close()
	err = <-serveErr
	if err != nil {
		if !strings.Contains(err.Error(), "use of closed network") {
			t.Error(err)
		}
	} else {
		t.Errorf("Expected closed network connection, got %v", err)
	}
}

func newServer(mux *http.ServeMux, address string) *http.Server {
	return &http.Server{
		Addr:           address,
		Handler:        mux,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}
}
