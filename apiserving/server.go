package apiserving

import (
	"io"
	"log"
	"net/http"
	"strings"
)

type grpcHttpApiServer struct {
	builder  *grpcHttpApiServerBuilder
	insecure *insecureServer
	secure   *secureServer
}

func newGrpcHttpApiServer() *grpcHttpApiServer {
	return &grpcHttpApiServer{
		builder: newGrpcHttpApiServerBuilder(),
	}
}

func (this *grpcHttpApiServer) SetHttpPort(port int) ApiServer {
	this.builder.httpPort = port
	return this
}

func (this *grpcHttpApiServer) SetHttpsPort(port int) ApiServer {
	this.builder.httpsPort = port
	return this
}

func (this *grpcHttpApiServer) SetGrpcPort(port int) ApiServer {
	this.builder.insecureGrpcPort = port
	return this
}

func (this *grpcHttpApiServer) SetTlsGrpcPort(port int) ApiServer {
	this.builder.tlsGrpcPort = port
	return this
}

func (this *grpcHttpApiServer) SetRootCertificateFromFile(rootCaPemPath string) ApiServer {
	this.builder.rootCaPemPath = rootCaPemPath
	return this
}

func (this *grpcHttpApiServer) SetCertificatesFromFile(serverPublicKeyPemPath, serverPrivateKeyPemPath string) ApiServer {
	this.builder.serverPublicKeyPemPath = serverPublicKeyPemPath
	this.builder.serverPrivateKeyPemPath = serverPrivateKeyPemPath
	return this
}

func (this *grpcHttpApiServer) SetRootCertificateFromPEM(rootCaPem []byte) ApiServer {
	this.builder.rootCaPem = rootCaPem
	return this
}

func (this *grpcHttpApiServer) SetCertificatesFromPEM(serverPublicKeyPem, serverPrivateKeyPem []byte) ApiServer {
	this.builder.serverPublicKeyPem = serverPublicKeyPem
	this.builder.serverPrivateKeyPem = serverPrivateKeyPem
	return this
}

func (this *grpcHttpApiServer) Add(registerFunc GrpcRegistrationFunc) ApiServer {
	this.AddInsecure(registerFunc)
	this.AddSecure(registerFunc)
	return this
}

func (this *grpcHttpApiServer) AddInsecure(registerFunc GrpcRegistrationFunc) ApiServer {
	this.builder.insecureRegs = append(this.builder.insecureRegs, registerFunc)
	return this
}

func (this *grpcHttpApiServer) AddSecure(registerFunc GrpcRegistrationFunc) ApiServer {
	this.builder.secureRegs = append(this.builder.secureRegs, registerFunc)
	return this
}

func (this *grpcHttpApiServer) AddRest(registerFunc GrpcRestRegistrationFunc) ApiServer {
	this.AddRestInsecure(registerFunc)
	this.AddRestSecure(registerFunc)
	return this
}

func (this *grpcHttpApiServer) AddRestInsecure(registerFunc GrpcRestRegistrationFunc) ApiServer {
	this.builder.insecureRestRegs = append(this.builder.insecureRestRegs, registerFunc)
	return this
}

func (this *grpcHttpApiServer) AddRestSecure(registerFunc GrpcRestRegistrationFunc) ApiServer {
	this.builder.secureRestRegs = append(this.builder.secureRestRegs, registerFunc)
	return this
}

func (this *grpcHttpApiServer) AddHandler(pattern string, handlerFunc http.HandlerFunc) ApiServer {
	this.AddHandlerInsecure(pattern, handlerFunc)
	this.AddHandlerSecure(pattern, handlerFunc)
	return this
}

func (this *grpcHttpApiServer) AddHandlerInsecure(pattern string, handlerFunc http.HandlerFunc) ApiServer {
	this.builder.insecureHttpHandlers = append(this.builder.insecureHttpHandlers, httpHandlerParams{
		pattern:     pattern,
		handlerFunc: handlerFunc,
	})
	return this
}

func (this *grpcHttpApiServer) AddHandlerSecure(pattern string, handlerFunc http.HandlerFunc) ApiServer {
	this.builder.secureHttpHandlers = append(this.builder.secureHttpHandlers, httpHandlerParams{
		pattern:     pattern,
		handlerFunc: handlerFunc,
	})
	return this
}

func (this *grpcHttpApiServer) AddHttpHandler(pattern string, handler http.Handler) ApiServer {
	this.AddHttpHandlerInsecure(pattern, handler)
	this.AddHttpHandlerSecure(pattern, handler)
	return this
}

func (this *grpcHttpApiServer) AddHttpHandlerInsecure(pattern string, handler http.Handler) ApiServer {
	this.builder.insecureHttpHandlers = append(this.builder.insecureHttpHandlers, httpHandlerParams{
		pattern: pattern,
		handler: handler,
	})
	return this
}

func (this *grpcHttpApiServer) AddHttpHandlerSecure(pattern string, handler http.Handler) ApiServer {
	this.builder.secureHttpHandlers = append(this.builder.secureHttpHandlers, httpHandlerParams{
		pattern: pattern,
		handler: handler,
	})
	return this
}

func (this *grpcHttpApiServer) EnableDebug() ApiServer {
	this.EnableDebugInsecure()
	this.EnableDebugSecure()
	return this
}

func (this *grpcHttpApiServer) EnableDebugSecure() ApiServer {
	this.builder.debugSecure = true
	return this
}

func (this *grpcHttpApiServer) EnableDebugInsecure() ApiServer {
	this.builder.debugInsecure = true
	return this
}

func (this *grpcHttpApiServer) SetSwaggerString(swaggerString string) ApiServer {
	this.builder.swaggerHttpHandler = func(w http.ResponseWriter, req *http.Request) {
		io.Copy(w, strings.NewReader(swaggerString))
	}
	return this
}

func (this *grpcHttpApiServer) EnableMetrics() ApiServer {
	this.builder.enablePrometheus = true
	return this
}

func (this *grpcHttpApiServer) BeginServingInsecure() error {
	var err error
	if this.insecure == nil {
		this.insecure, err = buildInsecure(this.builder)
		if err != nil {
			return err
		}
	}
	if this.insecure == nil {
		return nil
	} else {
		return this.insecure.beginServing()
	}
}

func (this *grpcHttpApiServer) BeginServingSecure() error {
	var err error
	if this.secure == nil {
		this.secure, err = buildSecure(this.builder)
		if err != nil {
			return err
		}
	}
	if this.secure == nil {
		return nil
	} else {
		return this.secure.beginServing()
	}
}

func (this *grpcHttpApiServer) BeginServing() error {
	err := this.BeginServingInsecure()
	if err != nil {
		return err
	}
	return this.BeginServingSecure()
}

func (this *grpcHttpApiServer) WaitForServing() error {
	err := this.WaitForServingInsecure()
	if err != nil {
		return err
	}
	return this.WaitForServingSecure()
}

func (this *grpcHttpApiServer) WaitForServingInsecure() error {
	if this.insecure != nil {
		this.insecure.waitForServing()
	}
	return nil
}

func (this *grpcHttpApiServer) WaitForServingSecure() error {
	if this.secure != nil {
		this.secure.waitForServing()
	}
	return nil
}

func (this *grpcHttpApiServer) StopServingInsecure() error {
	if this.insecure != nil {
		return this.insecure.stopServing()
	}
	return nil
}

func (this *grpcHttpApiServer) StopServingSecure() error {
	if this.secure != nil {
		return this.secure.stopServing()
	}
	return nil
}

func (this *grpcHttpApiServer) StopServing() error {
	err := this.StopServingInsecure()
	if err != nil {
		return err
	}
	return this.StopServingSecure()
}

func (this *grpcHttpApiServer) ServeAndWaitForever() error {
	err := this.BeginServing()
	if err != nil {
		return err
	}
	return this.Wait()
}

func (this *grpcHttpApiServer) Wait() error {
	err := this.insecure.waitForTermination()
	if err != nil {
		log.Printf("Error returned on Wait: %v", err)
	}
	err = this.secure.waitForTermination()
	return err
}
