package apitesting

import (
	"crypto/tls"
	"crypto/x509"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
	"testing"
)

func HttpGetStr(url string) (string, error) {
	log.Printf("HTTP GET %s", url)
	body, err := HttpGet(url)
	return string(body), err
}

func HttpGet(url string) ([]byte, error) {
	resp, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	return ioutil.ReadAll(resp.Body)
}

func HttpsGetStr(url string, pool *x509.CertPool) (string, error) {
	resp, err := HttpsGet(url, pool)
	return string(resp), err
}

func HttpsGet(url string, pool *x509.CertPool) ([]byte, error) {
	tr := &http.Transport{
		TLSClientConfig:    &tls.Config{RootCAs: pool},
		DisableCompression: true,
	}
	client := &http.Client{Transport: tr}
	resp, err := client.Get(url)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	return ioutil.ReadAll(resp.Body)
}

func HttpGetCompareBody(t *testing.T, url string, containsMatchers ...string) {
	body, err := HttpGetStr(url)
	if err != nil {
		t.Errorf("%s = %v", url, err)
	}
	if len(containsMatchers) <= 0 {
		t.Errorf("compareBody requires at least 1 match, got %d for %s", len(containsMatchers), url)
	}

	for _, containsMatcher := range containsMatchers {
		if !strings.Contains(body, containsMatcher) {
			t.Errorf("%s should contain %s = %v", url, containsMatcher, body)
		}
	}
}

func HttpsGetCompareBody(t *testing.T, url string, pool *x509.CertPool, containsMatchers ...string) {
	body, err := HttpsGetStr(url, pool)
	if err != nil {
		t.Errorf("%s = %v", url, err)
	}
	if len(containsMatchers) <= 0 {
		t.Errorf("compareBody requires at least 1 match, got %d for %s", len(containsMatchers), url)
	}

	for _, containsMatcher := range containsMatchers {
		if !strings.Contains(body, containsMatcher) {
			t.Errorf("%s should contain %s = %v", url, containsMatcher, body)
		}
	}
}
