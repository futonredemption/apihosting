package apitesting

import (
	"strings"
	"testing"
)

func assertContains(t *testing.T, big string, part string) {
	if !strings.Contains(big, part) {
		t.Errorf("%s does not contain %s", big, part)
	}
}

func TestConsts(t *testing.T) {
	// TODO: Verify that the certificates are valid.
	assertContains(t, ROOT_PUB_PEM, "CERTIFICATE")
	assertContains(t, ROOT_PRIV_PEM, "RSA PRIVATE KEY")
	assertContains(t, SERVER_PUB_PEM, "CERTIFICATE")
	assertContains(t, SERVER_PRIV_PEM, "RSA PRIVATE KEY")
}
