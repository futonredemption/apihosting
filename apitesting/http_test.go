package apitesting

import (
	"strings"
	"testing"
)

func assertContainsWithError(t *testing.T, big string, err error, part string) {
	if err != nil {
		t.Errorf("Unexpected Error: %s", err)
		return
	}
	if !strings.Contains(big, part) {
		t.Errorf("%s does not contain %s", big, part)
	}
}

func TestHttpMethods(t *testing.T) {
	str, err := HttpGetStr("http://www.futonredemption.com/")
	assertContainsWithError(t, str, err, "futon redemption")
}
