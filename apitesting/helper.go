package apitesting

import (
	"io/ioutil"
	"os"
	"path/filepath"
	"strconv"
	"sync"
	"sync/atomic"
)

var portMutex sync.Mutex
var port atomic.Value

func CreateTestDir() (string, func()) {
	dir, err := ioutil.TempDir("", "app_test")
	if err != nil {
		panic(err)
	}
	return dir, func() {
		os.RemoveAll(dir)
	}
}

func CreateTestFile() (string, func()) {
	dir, deferred := CreateTestDir()
	fn := filepath.Join(dir, "testfile")
	return fn, deferred
}

func GenHostsForCert(port int) string {
	hosts := "localhost"
	for i := port; i < port+5; i++ {
		hosts += ",:" + strconv.Itoa(i)
	}
	return hosts
}

func init() {
	port.Store(20000)
}

func NextPort() int {
	portMutex.Lock()
	defer portMutex.Unlock()
	p := port.Load().(int)
	port.Store(p + 5)
	return p
}
