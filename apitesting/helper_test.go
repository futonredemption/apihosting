package apitesting

import (
	"sync"
	"testing"
)

func TestNextPort(t *testing.T) {
	var wg sync.WaitGroup
	var mu sync.Mutex
	m := make(map[int]bool)
	expectedSlots := 50
	wg.Add(expectedSlots)
	for i := 0; i < expectedSlots; i++ {
		go func() {
			defer wg.Done()
			p := NextPort()
			mu.Lock()
			defer mu.Unlock()
			m[p] = true
		}()
	}
	wg.Wait()
	if len(m) != expectedSlots {
		t.Errorf("Expected %d but had %d ports allocated.", expectedSlots, len(m))
	}
}
